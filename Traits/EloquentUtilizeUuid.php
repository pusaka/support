<?php

namespace Pusaka\Support\Traits;

use Ramsey\Uuid\Uuid;

/**
 * UtilizeUuid Trait
 */
trait EloquentUtilizeUuid
{
    public static function bootEloquentUtilizeUuid()
    {
        static::creating(function ($model) {
            if (empty($model->attributes['uuid'])) {
                $model->generateUuid();
            }
        });
    }

    public function generateUuid()
    {
        $this->uuid = Uuid::uuid4();
    }

    public function getUuidAttribute()
    {
        return empty($this->attributes['uuid'])
            ? Uuid::NIL
            : $this->attributes['uuid'];
    }

    public function scopeByUuid($query, string $uuid)
    {
        return $query->where('uuid', $uuid);
    }
}
